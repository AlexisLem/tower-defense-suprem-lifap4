#ifndef DRAGON_H
#define DRAGON_H
#include <SFML/Graphics.hpp>


class dragon : public sf::Sprite
{
    public:
        /** @brief constructeur de la classe dragon*/
        dragon();

        /** @brief destructeur de la classe dragon*/
        virtual ~dragon();

        /** @brief Tableau de texture qui contiendra les trois textures de dragon, apparence du dragon change en fonction de ses points de vie */
        /** @param (Texture): contient les 3 textures de dragon  */
        sf::Texture texture[3];

        /** @brief Rectangle qui contiendra qui formera la lifebar */
        /** @param (greenBar) : Rectangle vert qui fait partie du lifebar du dragon */
        /** @param (redBar) : Rectangle rouge qui fait partie du lifebar du dragon  */
        sf::RectangleShape greenBar,
                           redBar;

        /** @brief Variables représentants la santé, santé maximun et la vitesse de déplacement des dragons */
        /** @param (health) : Santé actuelle */
        /** @param (full_health) : Santé max qui est censé rester constant  */
        /** @param (speed) : Vitesse du dragon qui peut être changée  */
        float health,
              full_health,
              speed;

        /** @brief Variable booléenne pour la mort des monstres */
        /** @param (is_dead) : si les dragons sont morts alors = 1 */
        bool is_dead;

        /** @brief Procédure pour charger les textures du dragon */
        void load_texture ( );

        /** @brief  Procédure pour afficher la barre de santé */
        /** @param (sf::RenderWindow &window): fenêtre de jeu */
        void draw_healthbar( sf::RenderWindow &window );

        /** @brief Procédure pour changer la texture du dragon en fonction de ses points de vie*/
        /** @param */
        void update_dragon( );
};

#endif // DRAGON_H
