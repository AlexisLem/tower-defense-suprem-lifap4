#include "terrain.h"

terrain::terrain(){
    terrain_texture_id_matrix.resize( 10 ) ;
    sprite_matrix.resize( 10 );
    texture_matrix.resize( 10 );

    for( int i = 0; i < 10; ++i ){
        terrain_texture_id_matrix[i].resize( 15 );
        sprite_matrix[i].resize( 15 );
        texture_matrix[i].resize( 15 );
    }

}

terrain::~terrain(){
    texture_list.clear();
    terrain_texture_id_matrix.clear();
    sprite_matrix.clear();
    texture_matrix.clear();
}

void terrain::load_terrain_info( std::string path ){

    std::ifstream f(path);
    if( f.is_open() ){
        std::string line;
        int j = 0;
        while( getline( f, line ) ){
            char id[15][5];
            //std::string id[15];
            if( sscanf( line.c_str(), "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ", id[0], id[1], id[2], id[3], id[4]
                                                                                     , id[5], id[6], id[7], id[8], id[9]
                                                                                     , id[10], id[11], id[12], id[13], id[14]) == 15 ){
                for( int i = 0; i < 15; i++ ){
                    terrain_texture_id_matrix[j][i] = id[i];
                }
                j++;
            }
            else{
                printf("invalid terrain data.\n");
            }
        }
        printf("terrain data loaded.\n");
    }
    f.close();
}

void terrain::load_texture_list( std::string path ){

    std::ifstream f(path);
    if( f.is_open() ){
        int tex_count = 0;
        std::string line;
        while( getline( f, line ) ){
            char tex_id[5], tex_path[50];
            if( sscanf( line.c_str(), "%s\t%s", &tex_id, &tex_path ) == 2 ){
                texture_list.push_back( { tex_id, tex_path } );

                ++tex_count;
            }
            else{
                printf("texture path not found, skipping >>\n");
            }
        }
        printf("%d terrain textures loaded.\n",tex_count);
    }
    f.close();
}

void terrain::load_textures( ){
    load_terrain_info( "data/terrain/pleasant_lake" );
    load_texture_list( "data/texture/texture_list" );

    for( std::size_t j = 0; j < 10 ; j++ ){
        for( std::size_t i = 0; i < 15 ; i++ ){
            for( std::size_t k = 0; k < texture_list.size(); k++ ){
                if( terrain_texture_id_matrix[j][i] == texture_list[k].texture_id ){
                    texture_matrix[j][i].loadFromFile( "data/texture/" + texture_list[k].texture_path );
                    sprite_matrix[j][i].setTexture( texture_matrix[j][i] );
                    sprite_matrix[j][i].setPosition( i * 48, j * 48 );
                }
            }
        }
    }
    features.load_textures();
}

void terrain::draw_terrain( sf::RenderWindow &w ){
    for( int j = 0; j < 10; j++ ){
        for( int i = 0; i < 15; i++ ){
            w.draw( sprite_matrix[j][i] );
        }
    }
    features.draw_features( w );
}
