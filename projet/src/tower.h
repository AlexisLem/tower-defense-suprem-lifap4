#ifndef TOWER_H
#define TOWER_H
#include <SFML/Graphics.hpp>
#include "dragon.h"

class tower: public sf::Sprite
{
    public:
        tower();
        virtual ~tower();

        /** @brief  création de la texture du terrain et du blast(eclat de tir)*/
        /** @param (tower_texture) : texture de la tour  */
        /** @param (blast_texture) : texture du blast  */
        sf::Texture tower_texture,
                    blast_texture;

        /** @brief creation du cercle représentant la pportée de tir */
        /** @param (reach_circle) : cercle représentnt la portée de tir  */
        sf::CircleShape reach_circle;

        /** @brief  création du clock servant à la régulation du temps pendant le tir*/
        /** @param (cooldown) : horloge qui va etre utilisé pour gérer le temps entre les tirs  */
        sf::Clock cooldown;

        /** @brief  création du sprite blast_sprite cui correstpond aux ecrats de tir*/
        /** @param (blast_sprite) : sprite correspondant au blast  */
        sf::Sprite blast_sprite;

        /** @brief  variables utilisés pour gerer le drag and drop */
        /** @param (is_being_dragged) : variable booleenne qui est à 1 si la tour est entrain d'etre deplacee  */
        /** @param (is_movable) : variable booleenne qui est à 1 si la tour est déplacable ( car une tour placée n'est plus déblacable )  */
        bool is_being_dragged,
             is_movable;

        /** @brief  Dégats de la tour */
        /** @param (TOWER_DAMAGE) : cont float représentant le taux de dégat que fait la tour à chaque tir  */
        const float TOWER_DAMAGE = 5;

        /** @brief  Procedure pour faire des dégats sur les monstres */
        /** @param (std::vector <dragon> &dragon_vect): vector correspondant au dragon */
        /** @param (sf::RenderWindow &window): fenetre de jeu */
        /** @param (unsigned int &golds): l'argend disponible est appele pour l'augmenter si le dragon est tué */
        void fire_at_monster( std::vector <dragon> &dragon_vect, sf::RenderWindow &window , unsigned int &golds);

        /** @brief  Fonction calculer la distance parcourir */
        /** @param (Vector2f) */
        float calc_distance( sf::Vector2f );
};

#endif // TOWER_H
