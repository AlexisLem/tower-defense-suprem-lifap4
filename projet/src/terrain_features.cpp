#include "terrain_features.h"

terrain_features::terrain_features()
{
    //ctor
}

terrain_features::~terrain_features()
{
    //dtor
}

void terrain_features::load_feature_info( std::string path ){
    std::ifstream f(path);
    if( f.is_open() ){
        std::string line;
        int j = 0;
        while( getline( f, line ) ){
            char id[15][5];
            if( sscanf( line.c_str(), "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ", id[0], id[1], id[2], id[3], id[4]
                                                                                     , id[5], id[6], id[7], id[8], id[9]
                                                                                     , id[10], id[11], id[12], id[13], id[14]) == 15 ){
                for( int i = 0; i < 15; i++ ){
                    feature_texture_id_matrix[i][j] = id[i];
                }
                j++;
            }
            else{
                printf("invalid feature data.\n");
            }
        }
        printf("feature data loaded.\n");
    }
}

void terrain_features::load_texture_list( std::string path ){
    std::ifstream f(path);
    if( f.is_open() ){
        int tex_count = 0;
        std::string line;
        while( getline( f, line ) ){
            char tex_id[5], tex_path[50];
            if( sscanf( line.c_str(), "%s\t%s", &tex_id, &tex_path ) == 2 ){
                texture_list.push_back( { tex_id, tex_path } );
                ++tex_count;
            }
            else{
                printf("texture path not found. skipping >>\n");
            }
        }
        printf("%d feature textures loaded.\n",tex_count);
    }
}

void terrain_features::load_textures( ){
    load_feature_info( "data/terrain/pleasant_lake_features" );
    load_texture_list( "data/texture/feature_list" );

    for( std::size_t j = 0; j < 10 ; j++ ){
        for( std::size_t i = 0; i < 15 ; i++ ){
            for( std::size_t k = 0; k < texture_list.size(); k++ ){
                if( feature_texture_id_matrix[i][j] == texture_list[k].texture_id/* && feature_texture_id_matrix[i][j] != "0000" */){
                    texture_matrix[i][j].loadFromFile( "data/texture/" + texture_list[k].texture_path );
                    sprite_matrix[i][j].setTexture( texture_matrix[i][j] );
                    sprite_matrix[i][j].setPosition( i * 48, j * 48 );
                }
            }
        }
    }
}

void terrain_features::draw_features( sf::RenderWindow &w ){
    for( int j = 0; j < 10; j++ ){
        for( int i = 0; i < 15; i++ ){
            w.draw( sprite_matrix[i][j] );
        }
    }
}
