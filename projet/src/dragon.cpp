#include "dragon.h"

dragon::dragon()
{
    greenBar.setSize( { 20, 4 } );
    greenBar.setFillColor( sf::Color::Green );
    redBar.setSize( { 20, 4 } );
    redBar.setFillColor( sf::Color::Red );

    full_health = 100.0f;
    health = 100.0f;
    speed = 3.0f;
    is_dead = false;
}

dragon::~dragon()
{
    //dtor
}

void dragon::load_texture( ){
    std::string dragon0 = "data/texture/Dragon.png",
                dragon1 = "data/texture/DragonAttack.png",
                dragon2 = "data/texture/DragonInDarkness.png";
    texture[0].loadFromFile( dragon0 );
    texture[1].loadFromFile( dragon1 );
    texture[2].loadFromFile( dragon2 );
    setTexture( texture[0] );
}

void dragon::draw_healthbar( sf::RenderWindow &window ){
    greenBar.setPosition( getPosition().x + 20, getPosition().y + 5 );
    redBar.setPosition( getPosition().x + 20, getPosition().y + 5 );

    greenBar.setSize( {20 * health / full_health, 4} );
    window.draw( redBar );
    window.draw( greenBar );
}

void dragon::update_dragon( ){
    if( 0.f < health && health < 66.f ){
        setTexture( texture[1] );
    }
    else if ( health <= 33 ){
        setTexture( texture[2] );
    }

}

// void dragon::set_max_health( float max_health ){
//     full_health = max_health;
//     health = max_health;
// }
