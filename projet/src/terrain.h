#ifndef TERRAIN_H
#define TERRAIN_H
#include <SFML/Graphics.hpp>
#include <fstream>
#include <vector>
#include "terrain_features.h"

class terrain
{
        struct texture_info { std::string texture_id, texture_path; };
        terrain_features features;
    public:
        terrain();
        virtual ~terrain();

        /** @brief  création d'un vector qui conserve les informations chargées à partir du fichier texture_list  */
        /** @param (texture_info): info de texture */
        /** @param (texture_list): nom du vector qui comtiendra les texture_info */
        std::vector <texture_info> texture_list;

        /** @brief terrain_texture_id_matrix conserve les ids de texture(string) sous la forme d'une matrice 10x15 pour pouvoir le dessiner */
        /** @param (terrain_texture_id_matrix): nom de la matrice */
        std::vector <std::vector <std::string>> terrain_texture_id_matrix;

        /** @brief sprite_matrix est in vector qui conserve les srpites pour pouvoir le dessiner */
        /** @param (sprite_matrix): nom de lu vector */
        std::vector <std::vector <sf::Sprite>> sprite_matrix;

        /** @brief texture_matrix garde la texture de chaque sprite dans sprite_matrix */
        /** @param (Texture): type de la variable stocke dans la matrice cree avec deux vector */
        /** @param (texture_matrix): nom de la matrice */
        std::vector <std::vector <sf::Texture>> texture_matrix;

        /** @brief procédure pour charger les informations du terrain */
        /** @param (path): string contenant le chemin pour le load */
        void load_terrain_info( std::string path );

        /** @brief Fprocédure pour charger la liste de texture and leur ids */
        /** @param (path): string contenant le chemin pour le load */
        void load_texture_list( std::string path );

        /** @brief procédure qui regroupe les procedures de chargement necessaire pour le terrain */
        void load_textures( );

        /** @brief Affiche la matrice de sprite */
        /** @param (RenderWindow &w): fenetre de jeu */
        void draw_terrain( sf::RenderWindow &w );
};

#endif // TERRAIN_H
