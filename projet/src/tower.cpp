#include "tower.h"
#include <math.h>

tower::tower()
{
    is_being_dragged = false;
    is_movable = true;
    tower_texture.loadFromFile( "data/texture/tower.png" );
    blast_texture.loadFromFile( "data/texture/blast.png" );
    setTexture( tower_texture );
    blast_sprite.setTexture( blast_texture );
}

tower::~tower()
{
    //dtor
}

void tower::fire_at_monster( std::vector <dragon> &vect , sf::RenderWindow &window, unsigned int &golds){
    int closest_monster_index = 1000;
    float min_distance = 1e35;
    for( std::size_t i = 0; i < vect.size(); i++ ){
        if( vect[i].health > 0 ) {
            sf::Vector2f dragon_center = { vect[i].getPosition().x + 24, vect[i].getPosition().y + 24 };
            float distance = calc_distance( dragon_center );
            if( distance < min_distance  ){
                closest_monster_index = i;
                min_distance = distance;
            }
        }
    }

    if( calc_distance( { (float)(vect[closest_monster_index].getPosition().x + 24.f),
        (float)(vect[closest_monster_index].getPosition().y + 24.f) } ) < reach_circle.getRadius() ){
        if( vect[closest_monster_index].health > 0.f ) {
            if( cooldown.getElapsedTime().asMilliseconds() > 1000 ){
                vect[closest_monster_index].health -= TOWER_DAMAGE;
                if ( vect[closest_monster_index].health <= 0.f ) golds += 5;
                cooldown.restart();
            }
            else if ( cooldown.getElapsedTime().asMilliseconds() < 500 ){
                blast_sprite.setPosition( getPosition().x + 19.f, getPosition().y +20.f );
                window.draw( blast_sprite );
            }
        }
    }
}

float tower::calc_distance( sf::Vector2f pos ){
    sf::Vector2f center = { (float)(getPosition().x + 24.0), (float)(getPosition().y + 24.0) };
    float distance = sqrt( (center.x - pos.x) * ( center.x - pos.x ) + ( center.y - pos.y ) * ( center.y - pos.y ) );
    return distance;
}
