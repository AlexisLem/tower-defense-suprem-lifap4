#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "terrain.h"
#include "tower.h"
#include "dragon.h"
#include <vector>
#include <fstream>


class game_window : public sf::RenderWindow
{
  /** @brief  creation d'une variable fenetre */
    sf::RenderWindow window;

    /** @brief Variable pour la largeur de fenêtre */
    /** @param (720): 720 de largeur */
    int const GAME_WINDOW_WIDTH = 720;

    /** @brief Variable pour la hauteur de fenêtre */
    /** @param (480): 480 de hauteur */
    int const GAME_WINDOW_HEIGHT = 480;

    /** @brief  Variable pour l'heure*/
    sf::Clock clock;

    /** @brief  Variable pour l'heure sous forme de texte */
    sf::Clock text_clock;

    /** @brief  Variable pour la police du texte*/
    sf::Font font;

    /** @brief  Variable pour l'affichage du texte en haut de la fenetre*/
    sf::Text header_text;

    /** @brief  Variable text pour afficher l'or restant*/
    sf::Text gold_text;

    /** @brief  creation de rectangles pour forner la lifebar du chateau */
    /** @param (castle_healthbar_green) : Rectangle vert qui fait partie du lifebar du chateau */
    /** @param (castle_healthbar_red) : Rectangle rouge qui fait partie du lifebar du chateau  */
    /** @param (castle_healthbar_black) : Rectangle noir qui fait partie du lifebar du chateau  */
    sf::RectangleShape castle_healthbar_red,
                       castle_healthbar_green,
                       castle_healthbar_black;

    /** @brief  Variable crecle pour afficher l'or */
    sf::CircleShape gold_shape;

    /** @brief  Variable pour créer un terrain */
    terrain terr;

    /** @brief  On stock les dragons dans un vector */
    /** @param (dragon) : type des donnees stckes dans le vector */
    std::vector <dragon> dragon_vector;

    /** @brief  On stock les tours dans une liste */
    /** @param (tower) : type des donnees stckes dans le vector */
    std::vector <tower> tower_vector;

    /** @brief  Variable pour le nombre de dragons maximun */
    int unsigned const NUMBER_OF_DRAGONS = 10;
    /** @brief  Variable pour le nombre de tours maximun */
    int unsigned const NUMBER_OF_TOWERS = 5;

    /** @brief  Variable pour stocker les golds stockés au cours de la partie */
    unsigned int golds;

    /** @brief  Variables pour la santé du chateau et sa santé max */
    float castle_health = 100,
          castle_full_health = 100;

    /** @brief  Variable pour gerer les différentes phases de jeu */
    int game_phase;

    public:
        /** @brief Procédure pour ouvrir la fenêtre du jeu */
        game_window( );

        /** @brief  Destructeur de game_window */
        virtual ~game_window( );

        /** @brief  Procédure pour afficher les informations sur la fenêtre */
        void set_hud( );

        /** @brief  Procédure pour initialiser le jeu */
        /** @param (title): Titre de la fenetre */
        void initialize_game( std::string title );

        /** @brief  Procédure pour charger le terrain */
        void load_terrain( );

        /** @brief  Procédure créer des monstres */
        void create_monsters( );

        /** @brief  Procédure pour le déplacement  des monstres */
        void move_monsters( );

        /** @brief  Procédure pour créer des tours */
        void create_towers( );

        /** @brief  Procédure qui contiendra la boucle principal pour faire fonctionner le jeu */
        void game_window_main_loop( );

        /** @brief  Procédure déplacer les tours */
        void drag_towers( );

        /** @brief  Procédure pour le tir des tours */
        void tower_fire( );

        /** @brief  Procédure pour recommencer le jeu */
        void restart_game( );

        /** @brief  Procédure pour faire apparaitre les tours */
        void draw_towers( );

        /** @brief  Procédure affficher du texte */
        void draw_text( );

        /** @brief  Procédure afficher les éléments de l'HUD (Affichage tête haute) */
        void draw_hud( );

        /** @brief  Procédure mettre à jour les éléments de l'HUD (Affichage tête haute) */
        void update_hud( );

        /** @brief  Procédure pour afficher les monstres */
        void draw_monsters( );
};

#endif // GAME_WINDOW_H
