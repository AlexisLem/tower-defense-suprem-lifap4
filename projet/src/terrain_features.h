#ifndef TERRAIN_FEATURES_H
#define TERRAIN_FEATURES_H
#include <SFML/Graphics.hpp>
#include <fstream>
#include <vector>
class terrain_features
{
        /** @brief  création d'une structure pour texture_info */
        /** @param (texture_id): id de la texture */
        /** @param (texture_path): chemin de la texture */
        struct texture_info { std::string texture_id, texture_path; };
    public:
        /** @brief  Procédure pour mettre en place les fonctionnalités du terrain */
        terrain_features();

        /** @brief  Destructeur de terrain_features */
        virtual ~terrain_features();

        /** @brief  vector conetenant les info de texture */
        /** @param (texture_info): type des elements du vector */
        std::vector <texture_info> texture_list;

        /** @brief  création d'un tableau à deux entrees qui contiendra les infos de feature_texture pour chaque tile de la map  */
        std::string feature_texture_id_matrix[15][10];

        /** @brief  Tableau de matrice qui contiendra les sprites des objets */
        sf::Sprite sprite_matrix[15][10];

        /** @brief  Matrice pour stocker les textures utilisés */
        sf::Texture texture_matrix[15][10];

        /** @brief procedure pour charger les informations */
        /** @param (path): chemin des infos */
        void load_feature_info( std::string path );

        /** @brief procedure pour charger la liste des textures */
        /** @param (string path): chemin de la liste */
        void load_texture_list( std::string path );

        /** @brief Procédure pour charger toutes les textures */
        void load_textures( );

        /** @brief procedure qui applique une feature texture */
        void set_feature_texture( );

        /** @brief procedure qui affichera les textures */
        /** @param (RenderWindow &w): affichage du rendu dans la fenêtre */
        void draw_features( sf::RenderWindow &w );
};

#endif // TERRAIN_FEATURES_H
