#include "game_window.h"
#include <time.h>
#include <string>

game_window::game_window()
{
    dragon_vector.resize( NUMBER_OF_DRAGONS );
    tower_vector.resize( NUMBER_OF_TOWERS );
    game_phase = 0;
    golds = 75;
}

game_window::~game_window()
{
    dragon_vector.clear();
    tower_vector.clear();
}

void game_window::initialize_game( std::string title ){
    window.create( sf::VideoMode(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT), title );
    load_terrain( );
    create_monsters( );
    create_towers( );
    set_hud( );
    game_window_main_loop( );
}

void game_window::load_terrain( ){
    terr.load_textures( );
}

void game_window::create_monsters( ){

    for( std::size_t i = 0; i < dragon_vector.size( ) ; i++ ){
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        srand( (time_t)ts.tv_nsec );
        dragon_vector[i].load_texture( );
        dragon_vector[i].setPosition( 235, - ((int)i * 100 + rand() % 100) );
    }
    //dragon_vector[ dragon_vector.size()-1 ].set_max_health( 200.0 );
    //dragon_vector[ dragon_vector.size()-1 ].setTexture( dragon_vector[ dragon_vector.size()-1 ].texture[2] );
}

void game_window::draw_monsters( ){
    for( std::size_t i = 0; i < dragon_vector.size( ) ; i++ ){
        if ( 0 < dragon_vector[i].health ) {
            dragon_vector[i].update_dragon( );
            window.draw( dragon_vector[i] );
            dragon_vector[i].draw_healthbar( window );
        }
    }
}

void game_window::move_monsters( ){
    if( clock.getElapsedTime().asMilliseconds() > 250 ){
        for( std::size_t i = 0; i < dragon_vector.size( ) ; i++ ){
            if( dragon_vector[i].health > 0 ){
                dragon_vector[i].move({ 0, dragon_vector[i].speed });
                if( dragon_vector[i].getPosition().y + dragon_vector[i].getGlobalBounds().height > window.getSize().y  ){
                    printf( "Castle damaged\n" );
                    castle_health -= 20;
                    dragon_vector[i].speed = 0;
                    dragon_vector[i].setPosition( 0, -1000 );
                    if( castle_health <= 0 ) game_phase = 0;
                }
            }
        }
        clock.restart();
    }
}

void game_window::create_towers( ){
    for( std::size_t i = 0; i < tower_vector.size(); i++ ){
        tower_vector[i].setPosition( { 48.f * 14.f, (float)i * 48.f } );
    }
}

void game_window::draw_towers( ){
    for( std::size_t i = 0; i < tower_vector.size(); i++ ){
        window.draw( tower_vector[i] );
    }
}

void game_window::drag_towers( ){
    static unsigned int selected_object = 100; /// This will prevent dragging multiple towers
    if( sf::Mouse::isButtonPressed( sf::Mouse::Left ) && golds >= 25 ){
        sf::FloatRect mouserect = { sf::Mouse::getPosition(window).x - 1.f,
                                    sf::Mouse::getPosition(window).y - 1.f,
                                    2, 2};
        for( std::size_t i = 0; i < tower_vector.size(); i++ ){
            if( tower_vector[i].getGlobalBounds().intersects(mouserect)  && tower_vector[i].is_movable ){
                selected_object = i;
                tower_vector[i].is_being_dragged = true;
            }
        }
        if ( selected_object < tower_vector.size() ){
            tower_vector[selected_object].setPosition( sf::Mouse::getPosition(window).x - 24,  sf::Mouse::getPosition(window).y -24 );
            tower_vector[selected_object].reach_circle.setRadius( 72 );
            tower_vector[selected_object].reach_circle.setPosition( sf::Mouse::getPosition(window).x - 72, sf::Mouse::getPosition(window).y - 60 );
            tower_vector[selected_object].reach_circle.setFillColor(sf::Color( 0.0f, 1.0f, 0.0f, 64 ));
            window.draw( tower_vector[selected_object].reach_circle );
        }
    }
    else{
        for( std::size_t i = 0; i < tower_vector.size(); i++ ){
            tower_vector[i].is_being_dragged = false;
        }
        if( selected_object < tower_vector.size() ){
        tower_vector[selected_object].is_movable = false;
            golds -= 25;
        }

        selected_object = 100;
    }
}

void game_window::tower_fire( ){
    for( std::size_t i = 0; i < tower_vector.size(); i++ ){
        for( unsigned int i = 0; i < dragon_vector.size( ) ; i++ ){
            tower_vector[i].fire_at_monster( dragon_vector, window , golds);
        }
    }
}

void game_window::draw_text( ){
    if( text_clock.getElapsedTime().asSeconds() < 5 ){
        window.draw( header_text );
    }
}

void game_window::restart_game( ){
    for( unsigned int i = 0; i < tower_vector.size( ) ; i++ ) {
        tower_vector[i].setPosition( { 48.f * 14.f, (float)i * 48.f } );
        tower_vector[i].is_movable = true;
    }
    text_clock.restart();
}

void game_window::set_hud( ){

    int const window_width = window.getSize().x,
              window_height = window.getSize().y;
    font.loadFromFile( "data/font/Perfect DOS VGA 437.ttf" );
    header_text.setFont( font );
    castle_healthbar_green.setFillColor( sf::Color::Green );
    castle_healthbar_green.setSize( { 80.f, 20.f } );
    castle_healthbar_green.setPosition( { window_width - 100.f, window_height - 40.f } );
    castle_healthbar_red.setFillColor( sf::Color::Red );
    castle_healthbar_red.setSize( { 80.f, 20.f } );
    castle_healthbar_red.setPosition( { window_width - 100.f, window_height - 40.f } );
    castle_healthbar_black.setFillColor( sf::Color::Black );
    castle_healthbar_black.setSize( { 84.f, 24.f } );
    castle_healthbar_black.setPosition( { window_width - 102.f, window_height - 42.f } );

    gold_shape.setPosition( { window_width - 100.f, window_height - 70.f } );
    gold_shape.setRadius( 10.0f );
    gold_shape.setOutlineColor( sf::Color::Black );
    gold_shape.setOutlineThickness( 2 );
    gold_shape.setFillColor( sf::Color(240, 204, 0) );

    gold_text.setFont( font );
    gold_text.setFillColor( sf::Color::Black );
    gold_text.setCharacterSize( 20 );
    gold_text.setPosition( { window_width - 70.f, window_height - 75.f } );
}

void game_window::draw_hud( ){
    update_hud( );
    window.draw( castle_healthbar_black );
    window.draw( castle_healthbar_red );
    window.draw( castle_healthbar_green );
    window.draw( gold_shape );
    window.draw( gold_text );
}

void game_window::update_hud( ){
    std::string gold_string = std::to_string(golds);
    gold_text.setString(gold_string);
    castle_healthbar_green.setSize( {80 *  castle_health/ castle_full_health, 20} );
}

void game_window::game_window_main_loop( ){
    while( window.isOpen() ){
        sf::Event evn;
        while( window.pollEvent( evn ) ){
            switch (evn.type){
                case sf::Event::Closed: {
                    window.close( );
                }
                case sf::Event::MouseButtonReleased : {
                    switch ( evn.mouseButton.button ){
                        case sf::Mouse::Left: {
                            header_text.setString( "Stop dragons before they destroy the city" ) ;
                                if( game_phase == 0 ){
                                    game_phase = 1;
                                }
                            break;
                        }
                        case sf::Mouse::Right : {
                            restart_game( );
                            break;
                        }
                        default:
                            break;
                    }
                    break;
                    default:
                        break;
                }
            }
        }
        if( game_phase == 0 ){
            window.clear( sf::Color::Green );
            terr.draw_terrain( window );
            header_text.setString( "Click to start" );
            draw_text( );
        }

        else if( game_phase == 1 ){
            window.clear( sf::Color::Black );
            terr.draw_terrain( window );
            draw_monsters( );
            draw_hud( );
            move_monsters( );
            drag_towers( );
            draw_towers( );
            tower_fire( );
            draw_text( );
        }
        window.display( );
    }
}
