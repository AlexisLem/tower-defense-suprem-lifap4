# Tower defense - LIFAP4

P1920184 : KAHRAMAN Mumin
P1914167 : DIJOUX Remi
p1307768 : Lemoine Alexis

Introduction:
Ce jeu est un "Tower Defense" et Le but est de défendre une zone contre une d’ennemis se déplaçant suivant un itinéraire, tout en placant des tours défensives.

Comment jouer:
- quand la fenetre s'ouvre vous devez cliquer dessus pour lancer la partie.
- les dragons vont commencer à arriver, vous devez placer les tours qui sont sur la droite sur le terrain en les glisser deposant
- placer une tour coute 25 pieces
- chaque dragon tué rapporte 5 pieces
- vous ne pouvez plus déplacer une tour placee mais vous pouvez remettre a zero les tours en cliquant droit sur l'ecran

Classes: 
1)classe dragon, crée un monstre (dragon).
2)classe game_window, crée une fenêtre pour afficher le jeu.
3)classe terrain, crée un terrain de jeu
4)classe terrain_features, pour ajouter des éléments de décors au terrain de base
5)classe tower, pour créer des tours qui serviront à défendre le château

Codes:
1) Code dragon.cpp, développe les fonctions et procédures de la classe dragon
2) Code game_window.cpp, développe les fonctions et procédures de la classe game_window
3) Code terrain.cpp, développe les fonctions et procédures de la classe terrain
4) Code terrain_features.cpp, développe les fonctions et procédures de la classe terrain_features
5) Code tower.cpp, développe les fonctions et procédures de la classe tower

Arborescence:
L'archive comporte plusieurs répertoires:
- le repertoire src, contient tous les fichiers .h et .cpp
- le repertoire bin, contenant les fichiers executables que le makefile à fait creer.
- le repertoire data, contient les images et autres elements travaillés par le programme pendant l'execution.
- le repertoire doc, contenant le fichier doxyfile et la documentation html.
- le repertoire obj, contenant les fichhiers objet.
- le repertoire SFML, contenant la librairie sfml
- le projet codeblocks au cas ou il y en aurait besoin

IMPORTANT : pour pouvoir compiler et lance le programme vous devez prealablement installer la librairie sfml sur votre machine linux.
Pour se faire vous pouvez utiliser la commande suivante : sudo apt-get install libsfml-dev


Compilation:
Pour compiler le projet, il suffit de lancer la commande "make" dans un terminal ouvert dans le  repertoire du projet. Pour l'éxécuter, il suffit d'entrer la commande " ./bin/tower-defense " dans le terminal ouvert dans le repertoire du projet.